import { createContext, useReducer } from "react";
const appContext   = createContext(null);
const initialState = {
    in: 0,
    out: 0,
    ledger: []
} 
function txn(value, purpose){
    return {
        type: "txn",
        value,
        purpose
    }
}
function reducer(state, action){
    switch(action.type){
        case "txn":
            let change = {in: state.in + action.value}
            if(action.value < 0){
                change = {out: state.out + action.value}
            }
            return {
                ...state,
                ...change,
                ledger: [...state.ledger, {purpose: action.purpose, value: action.value}]
            }
        case "restore":
            let cache = localStorage.getItem("expense-tracker-cache");
            if(cache){
                let cdata = JSON.parse(cache);
                return {...state, ...cdata}
            }
            return state;
        default:
            return state;
    }
}

function AppCtxProvider({children}){
    return (
        <appContext.Provider value={useReducer(reducer, initialState)}>
            {children}
        </appContext.Provider>
    )
}

export {
    appContext,
    AppCtxProvider,
    txn
}