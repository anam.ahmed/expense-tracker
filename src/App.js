import { useContext, useEffect } from "react";
import { appContext } from "./context";
import {BalanceView} from "./components/balance";
import { Overview } from "./components/overview";
import {History} from "./components/history";
import { AddExpense } from "./components/addexpense";
function App() {
  const [data, dispatch] = useContext(appContext);
  useEffect(()=>{
      dispatch({type: "restore"});
  },[]);
  useEffect(()=>{
    if(data) localStorage.setItem("expense-tracker-cache", JSON.stringify(data));
  }, [data]);
  return (
    <div className="App">
      <div className="container">
        <h2 className="app-title fg1">
          Expense Tracker
        </h2>
        <BalanceView/>
        <Overview/>
        <History/>
        <AddExpense/>
      </div>
    </div>
  );
}
export default App;
