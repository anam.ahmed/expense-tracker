import {useContext, useState, useCallback} from "react";
import swal from "sweetalert";
import { appContext, txn } from "../context";
function AddExpense(){
    const [amount, setAmount] = useState('');
    const [purpose, setPurpose] = useState('');
    const [data, dispatch] = useContext(appContext);
    const balance = data.in + data.out;
    const add = useCallback(()=>{
        if(!purpose.trim()) return swal("Oops!","Purpose cannot be empty!", "error");
        const _amount = parseFloat(amount);
        if(!_amount) return swal("Oops!", "Please enter a valid amount", "error");
        if(balance + _amount < 0) return swal("Insufficiant balance!","You don't have enough balance for this expense", "error");
        dispatch(txn(_amount, purpose));
        swal("Transaction added!", "Your transaction is added to your ledger", "info");
        setAmount('');
        setPurpose('');
    },[purpose, amount, balance]);

    return (
    <div className="expense-form">
        <h4 className="section-title fg2">Add new transaction</h4>
        <input className="fg2 bg2"
        onChange={e=>setPurpose(e.target.value)}
        value={purpose}
        type="text" placeholder="Purpose"/>
        <input
        value={amount}
        onChange={e=>setAmount(e.target.value)}
        className="fg2 bg2" type="number" placeholder="amount"/>
        <div className="hint fg2">
            negative for expense, positive for income.
        </div>
        <div onClick={add} className="button">Add transaction</div>
    </div>
    );
}

export {AddExpense}