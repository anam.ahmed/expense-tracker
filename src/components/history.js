import { appContext } from "../context";
import {useContext} from "react";
function HistoryItem({item}){
    const type = (item.value < 0)?"out":"in";
    return (
        <div className={`history-item bg2 ${type}`}>
            <h4 className="item-purpose fg2">
            {item.purpose}
            </h4>
            <h4 className="item-amount fg1">
               {item.value < 0? "-":"+"} ${Math.abs(item.value)}
            </h4>
        </div>
    )
}
function History(){
    const [data] = useContext(appContext);
    if(!data.ledger.length) return null;
    return (
    <div className="history-container">
        <h4 className="fg2 section-title">History</h4>
        {/* index is fine here as key
        since it wont change with the array change */}
        {data.ledger.map((_item, index)=><HistoryItem item={_item} key={index}/>)}
    </div>
    );
}

export {History}