import { useContext } from "react";
import { appContext } from "../context";

function Overview(){
    const [data, dispatch] = useContext(appContext);
    return (
        <div className="account-overview bg2">
            <div className="overview-content first">
                <h4 className="overview-title fg2">Income</h4>
                <h2 className="ov-amount money-in">${data.in}</h2>
            </div>
            <div className="overview-content">
                <h4 className="overview-title fg2">Expense</h4>
                <h2 className="ov-amount money-out">${Math.abs(data.out)}</h2>
            </div>
        </div>
    )
}

export {Overview}