import {useContext} from "react";
import {appContext} from "../context";

function BalanceView(){
    const [data] = useContext(appContext);
    return (
        <div className="balance-view">
            <div className="balance-texts">
                <h4 className="balance-healine fg2">
                    Your balance
                </h4>
                <h1 className="actual-balance fg1">
                    ${data.in + data.out}
                </h1>
            </div>
        </div>
    )
}

export {BalanceView}