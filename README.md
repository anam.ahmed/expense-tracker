# React Expense tracker sample project 
 Created from scratch using react, Context API, Testing library: Cypress.

> Please note: this app corresponds to the system theme. If the system has dark mode enabled, it will appear in dark mode.
## How to use
### Running the development server: 

1. you need to have [node.js](https://nodejs.org) 16 and [yarn](https://yarnpkg.com/) installed or later installed. 
2. Navigate to the project folder from command line and install dependencies by running the command `yarn`.
3. Once the dependencies are installed, run `yarn start`.
4. navigate to <http://localhost:4000> and you should see the app.

### Running the integration tests

1. Keep the dev server running (follow the instructions above).
2. Run `yarn test` to run the automated tests in command line. 
3. If you want to observe the integration tests visually, run `yarn test-visually`, and it will open the cypress interface. 

### Building for production

1. Make sure the dependencies are installed.
2. Run `yarn build` to build the application.
3. the `build` folder should contain the production build.
### Live demo: 

I've prepared a live demo here: <https://aa-expense-tracker.netlify.app>