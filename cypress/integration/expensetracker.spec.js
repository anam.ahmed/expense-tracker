function txn(_cy, amount, purpose){
  _cy.get('input[type=text]').type(purpose);
  _cy.get('input[type=number]').type(amount);
  _cy.get('.button').click();
  _cy.contains("OK").click();
}
describe('Expense tracker APP', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4000');
    cy.clearLocalStorage();
  });

  it("checks the layout",()=>{
    cy.contains("Expense Tracker");
    cy.contains("Your balance");
    cy.contains("Add transaction");
  });

  it("Adds transactions and checks balance", ()=>{
    txn(cy, 100, "Test Add");
    cy.get(".actual-balance").contains("$100");
    txn(cy, -50, "Test removal");
    cy.get(".actual-balance").contains("$50");
    cy.get(".money-in").contains("$100");
    cy.get(".money-out").contains("$50");
  });

  it("Checks history feature", ()=>{
    txn(cy, 13.44, "Test History Add");
    cy.get(".history-item .item-purpose").contains("History Add");
    cy.get(".history-item .item-amount").contains("$13.44");
  });

  it("checks local caching", ()=>{
    txn(cy, 200, "Test caching");
    cy.reload();
    cy.get(".actual-balance").contains("$200");
  })
});


